# Typhon

## Permitir o controle e a manutenção de computadores pela rede (acesso remoto).
  
  * Verificar quantos computadores estão conectados a uma central de distribuição
  * Ligar o computador via rede
  * Atualização da BIOS e mudar a senha do sistema
  * Escolher o sistema operacional
  * fazer a configuração e/ou instalação de aplicativos no sistema
  
## Ferramentas de acesso remoto

  * DESKTRA Freedom Desktop
  * Gbridge
  * LogMeIn Free Edition
  * Remote Desktop Connection
  * TeamViewer 

<http://www.landesk.com/>

<https://fogproject.org/>

![Group policy](./image/grouppolicy.jpg "Group policy")

| Function name | Description                    |
| ------------- | ------------------------------ |
| `help()`      | Display the __help__ window.   |
| `destroy()`   | **Destroy your computer!**     |
